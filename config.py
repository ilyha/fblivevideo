import os
import uuid
import sys

basedir = os.path.abspath(os.path.dirname(__file__))


# Form settings
CSRF_ENABLED = True
SECRET_KEY = '08919694-dd25-4a70-9e04-ac25173dc788'

# DB settings
if os.environ.get('DATABASE_URL') is None:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db') + '?check_same_thread=False'
else:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

# FB settings
if os.environ.get('FB_APP_ID') is None:
    print('Environment variable FB_APP_ID is not installed')
    sys.exit()
FB_APP_ID = os.environ.get('FB_APP_ID')

if os.environ.get('FB_APP_VERIFY_TOKEN') is None:
    print('Environment variable FB_APP_VERIFY_TOKEN is not installed')
    sys.exit()
FB_APP_VERIFY_TOKEN = os.environ.get('FB_APP_VERIFY_TOKEN')

if os.environ.get('FB_APP_SECRET') is None:
    print('Environment FB_APP_SECRET is not installed')
    sys.exit()
FB_APP_SECRET = os.environ.get('FB_APP_SECRET')


OAUTH_CREDENTIALS = {
    'facebook': {
        'id': FB_APP_ID,
        'secret': FB_APP_SECRET,
        'state': os.environ.get('FB_APP_STATE', '9b948b89-4218-44a7-9907-67aa5fe444e4')
    }
}

# pagination
POSTS_PER_PAGE = 5
