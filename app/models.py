from app import db
from flask_login import UserMixin

OperatorsStatus = ['new', 'active', 'blocked']
PostStatus = ['LIVE', 'VOD', 'DEL']


# StreamPlatforms = ['facebook']


class Operator(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    platform_id = db.Column(db.String(64), index=True, unique=True)
    nickname = db.Column(db.String(64), index=True)
    email = db.Column(db.String(120), index=True)
    access_token = db.Column(db.String(300), unique=True)
    status = db.Column(db.Enum(*OperatorsStatus, name='OperatorsStatus'), default=OperatorsStatus[0])
    last_seen = db.Column(db.DateTime)
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {0}>'.format(self.nickname)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)
    #operator_id = db.Column(db.Integer, db.ForeignKey('operator.id'))
    operator_id = db.Column(db.String(64), db.ForeignKey('operator.platform_id'))
    video_id = db.Column(db.String(40))
    status = db.Column(db.Enum(*PostStatus, name='PostStatus'), default=PostStatus[0])

    def __repr__(self):  # pragma: no cover
        return '<Post {0}>'.format(self.body)
