from app import app, lm, db
from flask_login import login_required, login_user, logout_user, current_user
from flask import request, abort, render_template, redirect, url_for, flash, g
from app.models import Operator, Post, PostStatus
from app.oauth import OAuthSignIn
from datetime import datetime, timedelta
from config import POSTS_PER_PAGE
import requests


@lm.user_loader
def load_user(uid):
    temp = Operator.query.get(int(uid))
    print('-----------------(', temp, ')-----------------')
    return temp


@app.route('/login')
def login():
    app.logger.info(
        'Function login, parameter g.user.is_authenticated = {}'.format(g.user.is_authenticated))
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In')


@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    app.logger.info(
        'Function oauth_authorize, parameter current_user.is_anonymous = {}'.format(current_user.is_anonymous))
    if not current_user.is_anonymous:
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@app.route('/callback/<provider>')
def oauth_callback(provider):
    if not current_user.is_anonymous:
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    platform_id, nickname, email, access_token = oauth.callback()
    if platform_id is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))
    user = Operator.query.filter_by(platform_id=platform_id).first()
    if not user:
        user = Operator(platform_id=platform_id, nickname=nickname, email=email,
                        access_token=access_token, status='new')
        db.session.add(user)
        db.session.commit()
        app.logger.info('Created operator: ' + user.nickname)
        user_live_videos = get_fb_video(user, 1)
        if user_live_videos:
            for video in user_live_videos['data']:
                if video['status'] in PostStatus[0:2]:
                    post = Post(body=video['embed_html'], timestamp=datetime.strptime(video['creation_time'],
                                                                                      "%Y-%m-%dT%H:%M:%S+0000"),
                                author=user, video_id=str(video['id']), status=video['status'])
                    db.session.add(post)
                    db.session.commit()
    login_user(user, True)
    return redirect(url_for('index'))


def get_fb_video(operator, limit=1):
    url = "https://graph.facebook.com/v2.8/{user_fb_id}/live_videos?access_token={access_token}\
            &limit={limit}&fields=id,+creation_time,+embed_html,+from,+status,+seconds_left".format(
        user_fb_id=operator.platform_id.split('$')[1], access_token=operator.access_token, limit=limit)

    r = requests.get(url)
    if r.status_code != 200:
        app.logger.error("Failed with code=", r.status_code, " to execute the query: ", url)
        app.logger.error("Server answer", r.text)
        return False
    if r.json().get('error') is not None:
        app.logger.error("Facebook server return an error: ", r.json()['error']['code'])
        app.logger.error("Server message", r.json()['error']['message'])
        app.logger.error("Parameter fbtrace_id", r.json()['error']['fbtrace_id'])
        return False
    return r.json()


@app.route('/webhook2017', methods=['GET', 'POST'])
def webhook():
    if request.method == 'GET':
        challenge = request.args.get('hub.challenge')
        if not challenge:
            app.logger.warning('Host {} visit this url without parameter hub.challenge'.format(request.remote_addr))
            abort(401)
        if request.args.get('hub.verify_token', '') == app.config['FB_APP_VERIFY_TOKEN']:
            app.logger.info('Facebook server {} check status'.format(request.remote_addr))
        else:
            app.logger.warning('Unknown host {} check status'.format(request.remote_addr))
        return challenge, 200
    else:
        # app.logger.info('Security info: ' + request.headers.get('X-Hub-Signature'))
        webhook_info = request.get_json()['entry']
        app.logger.info('Webhook:' + str(webhook_info))
        operator = Operator.query.filter_by(platform_id='facebook$' + webhook_info[0]['id']).first()
        video = get_fb_video(operator)
        if len(video['data']) == 1:
            local_post = Post.query.filter_by(video_id=video['data'][0]['id']).first()
            if not local_post and video['data'][0]['status'] == 'LIVE':
                if video['data'][0]['seconds_left'] == 14400:
                    app.logger.info('Video not started, waiting...')
                    return '', 426
                app.logger.info('Add new live video ' + str(video['data'][0]['id']))
                post = Post(body=video['data'][0]['embed_html'],
                            timestamp=datetime.strptime(video['data'][0]['creation_time'],
                                                        "%Y-%m-%dT%H:%M:%S+0000"), author=operator,
                            video_id=str(video['data'][0]['id']),
                            status='LIVE')
                db.session.add(post)
                db.session.commit()
            elif video['data'][0]['status'] == 'VOD' and local_post.status != 'VOD':
                app.logger.info('Live video and, set status VOD')
                local_post.status = 'VOD'
                db.session.commit()
            else:
                app.logger.info('Live video change unsupported status:' + video['data'][0]['status'])
        return '', 200


@app.route('/')
@app.route('/index')
@app.route('/index/<int:page>')
@login_required
def index(page=1):
    posts = g.user.posts.order_by(Post.id.desc()).paginate(page, POSTS_PER_PAGE, False)
    return render_template('index.html', title='Home', posts=posts)


@app.route('/scheduled/<email>/<period>')
def get_scheduled_video(email=None, period=None):
    if email is None or period is None:
        app.logger.info('Запрос запланированного видео с отсутствущим параметром время:{0} или почта:{1}'.format(period,email))
        return render_template('scheduled_error.html', error='Неверно заданы оператор или время вещания')
    datetime_list = period.split('-')
    if len(datetime_list) != 5:
        app.logger.info('Запрос запланированного видео с неверным параметром время:{0}'.format(period))
        return render_template('scheduled_error.html', error='Неверно задано время вещания')
    time_start = datetime(int(datetime_list[0]), int(datetime_list[1]), int(datetime_list[2]), int(datetime_list[3]))
    # if time_start > datetime.now():
    #     return render_template('static_error.html', 'Неверно задано время вещания')
    time_end = time_start + timedelta(hours=int(datetime_list[-1]))
    user = Operator.query.filter_by(email = email).first()
    if user is None:
        app.logger.info('Запрос запланированного видео с неверным параметром оператор:{0}'.format(email))
        return render_template('scheduled_error.html', error='Оператор с такими данными не зарегестрирован')
    post= Post.query.filter(Post.author==user).filter(Post.timestamp>=time_start,
        Post.timestamp<=time_end).order_by(Post.id.desc()).first()
    # post = user.posts.filter_by(timestamp>=time_start).filter_by(timestamp<=time_end).order_by(
    #     Post.id.desc()).first()
    if post is None:
        return render_template('scheduled_none.html')
    else:
        url_video = post.body.split('"')[1]
        app.logger.info('Пользователю: {0} был предоставлен доступ к видео {1}'.format(request.remote_addr, post.video_id))
        return redirect(url_video)


@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated:
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500
