# -*- coding: UTF-8 -*-
from rauth import OAuth2Service
from flask import current_app, url_for, request, redirect


class OAuthSignIn(object):
    providers = None

    def __init__(self, provider_name):
        self.provider_name = provider_name
        credentials = current_app.config['OAUTH_CREDENTIALS'][provider_name]
        self.consumer_id = credentials['id']
        self.consumer_secret = credentials['secret']
        self.state = credentials['state']

    def authorize(self):
        pass

    def callback(self):
        pass

    def get_callback_url(self):
        return url_for('oauth_callback', provider=self.provider_name,
                       _external=True,  _scheme='https')

    @classmethod
    def get_provider(self, provider_name):
        if self.providers is None:
            self.providers = {}
            for provider_class in self.__subclasses__():
                provider = provider_class()
                self.providers[provider.provider_name] = provider
        # print '\n\n', self.providers
        return self.providers[provider_name]


class FacebookSignIn(OAuthSignIn):
    def __init__(self):
        super(FacebookSignIn, self).__init__('facebook')
        self.service = OAuth2Service(
            name='facebook',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://graph.facebook.com/oauth/authorize',
            access_token_url='https://graph.facebook.com/oauth/access_token',
            base_url='https://graph.facebook.com/'
        )

    def authorize(self):
        return redirect(self.service.get_authorize_url(
            scope='email, user_posts, user_videos',
            # scope='email',
            response_type='code',
            redirect_uri=self.get_callback_url(),
            state=self.state)
        )

    def callback(self):
        if 'code' not in request.args:
            print('Error, program expects parameter code, but has not been received')
            return None, None, None, None
        if request.args.get('state') != self.state:
            print('State = ', self.state, 'but received', request.args.get('state'))
            return None, None, None, None
        access_token = self.service.get_access_token(method='POST', data={'code': request.args['code'],
                                                                          'grant_type': 'authorization_code',
                                                                          'redirect_uri': self.get_callback_url()})
        oauth_session = self.service.get_session(access_token)
        if self.service.access_token_response:
            oauth_session.access_token_response = self.service.access_token_response
        me = oauth_session.get('me?fields=id,email,name').json()
        return (
            'facebook$' + me['id'],
            me.get('name'),
            me.get('email'),
            access_token
        )
