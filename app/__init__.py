from flask import Flask
import logging
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager


app = Flask(__name__)
app.config.from_object('config')
app.config.update(dict(
  PREFERRED_URL_SCHEME='https'
))
db = SQLAlchemy(app)


stream_handler = logging.StreamHandler()
app.logger.addHandler(stream_handler)
app.logger.setLevel(logging.INFO)
app.logger.info('Server startup')

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
lm.login_message = 'Please log in to access this page.'

from app import views, models, oauth
