FROM python:3.6-alpine

RUN set -x && \
    mkdir /usr/live-video

COPY requirements.txt /tmp/

RUN pip install --no-cache-dir --disable-pip-version-check -r /tmp/requirements.txt

COPY . /usr/live-video
WORKDIR /usr/live-video

CMD ["gunicorn", "--bind", "0.0.0.0:8080", "runp:app"]
